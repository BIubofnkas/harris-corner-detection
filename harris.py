#!/usr/bin/python2



# Harris Corner Detection
# Liam Normoyle - 14177994
# Aaron Moloney - 14174014
# Kieran Peake  - 14155737

import numpy as np
import cv2
from PIL import Image
from scipy.ndimage import filters
import matplotlib.pylab as mpl
import math



def corner_detection_algorithm(image):
    Ix = np.zeros(image.shape)
    filters.gaussian_filter(image, 1, (0,1), Ix)

    Iy = np.zeros(image.shape)
    filters.gaussian_filter(image, 1, (1,0), Iy)

    A = filters.gaussian_filter(Ix**2, 2)
    B = filters.gaussian_filter(Ix*Iy, 2)
    C = filters.gaussian_filter(Iy*Ix, 2)
    D = filters.gaussian_filter(Iy**2, 2)

    determinate_matrix = (A * D) - (B * C)
    trace_matrix = (A + D)

    threshold = determinate_matrix / trace_matrix
    return threshold


def harris_corner_detection(harris_im):
    min_d = 10
    threshold = 0.1
    corner_threshold = harris_im.max () * threshold
    #print("Corner threshold: "),
    #print(corner_threshold)


    harris_im_th = (harris_im > corner_threshold)

    coords = np.array(harris_im_th.nonzero()).T

    candidate_values = np.array([harris_im[c[0],c[1]] for c in coords])
    #print("Candidate values: "),
    #print(candidate_values)


    indices = np.argsort(candidate_values)
    #print("Indice values: "),
    #print(indices)

    allowed_locations = np.zeros(harris_im.shape, dtype='bool')


    allowed_locations[  min_d:-min_d  ,  min_d:-min_d  ] = True




    filtered_coords = []
    for i in indices[::-1]:

        r,c = coords[i]

        if allowed_locations[r,c]:
            filtered_coords.append((r,c))
            allowed_locations[r-min_d:r+min_d+1,c-min_d:c+min_d+1] = False


    return filtered_coords


#Compose images:
def compose_images(im1, im2, translation):
    dims2 = np.shape(im2)
    dims1 = np.shape(im1)
    X1, Y1 = dims1
    X2, Y2 = dims2
    trans_X = translation[0]
    trans_Y = translation[1]
    composed_dims = (X1+trans_X, Y1+trans_Y)
    composed_img = np.zeros(composed_dims)
    composed_img[0:X1, 0:Y1] = im1
    composed_img[trans_X:X2+trans_X, trans_Y:Y2+trans_X,] = im2
    return composed_img


def image_patch_vector(hips, image):
    patch_vector = []
    for x in hips:
        row =  image[ x[0]-5: x[0]+6  , x[1]-5: x[1]+6]
        row = row.flatten()
        average = np.mean(row)
        row -= average # Get average frequency intensity for each point
        row /= np.linalg.norm(row) #Find the norm of point and divide
        patch_vector.append(row)
    return patch_vector


def show_images(arch1, arch2, composed_image):


    window1 = mpl.figure("Original Images")
    ax1 = window1.add_subplot(121)
    ax1.set_title("Original Image Arch1")
    ax1.imshow(arch1, cmap="gray")

    ax2 = window1.add_subplot(122)
    ax2.set_title("Original Image Arch2")
    ax2.imshow(arch2, cmap="gray")
    window1.show()


    window2 = mpl.figure("Harris Converted Images")
    bx1 = window2.add_subplot(121)
    bx1.set_title("Harris Converted Image Arch1")
    bx1.imshow(harris_converted_arch1, cmap="gray")

    bx2 = window2.add_subplot(122)
    bx2.set_title("Harris Converted Image Arch2")
    bx2.imshow(harris_converted_arch2, cmap="gray")
    window2.show()

    window3 = mpl.figure("Images With Interest Points")
    cx1 = window3.add_subplot(121)
    cx1.set_title("Arch1 With Interest Points")

    cx1.imshow(arch1, cmap="gray")
    for x in arch1_interest_points:
        mpl.scatter(x[1], x[0])


    cx2 = window3.add_subplot(122)
    cx2.set_title("Arch2 With Interest Points")
    cx2.imshow(arch2, cmap="gray")
    for x in arch2_interest_points:
        mpl.scatter(x[1], x[0])
    window3.show()


    mpl.figure("Images Composed On One Another")
    mpl.imshow(composed_image, cmap="gray")
    mpl.show()


def response_matrix(image_patch_vector_array_arch1, image_patch_vector_array_arch2):
    response_matrix = np.inner(image_patch_vector_array_arch1, image_patch_vector_array_arch2)
    response_matrix_s = np.argsort(response_matrix)
    response_th = (response_matrix_s.max() * 0.95)
    resp_im_th = (response_matrix > response_th)

    resp_im_th = np.zeros(resp_im_th, dtype='bool')


    resp_coords = np.array(resp_im_th.nonzero()).T

    print("Response matrix s: "),
    print(response_matrix_s)

    print("Response threshold: "),
    print(response_th)

    print("Response image threshold: "),
    print(resp_im_th)

    print("Response Coordinates: "),
    print(resp_coords)


    candidate_values = np.array([resp_coords[c[0]] for c in resp_coords])
    return resp_coords

def ransac(resp_coords):
    #EXHAUSTIVE RANSAC
    distance_max = 0
    distance_min_model = 1000000 #some high number
    for i in resp_coords:
        pt1 = resp_coords[i]
        for j in resp_coords:
            pt2 = resp_coords[j]
            m = (pt2[1]-pt1[1])/(pt2[0] - pt1[0]) #a = model slope
            c = pt0[1] - model_slope * pt0[0] #b = model intercept
            deno = math.sqrt(a*a + 1)

            for k in resp_coords:
                test_pt = resp_coords[k]
                distance = (m*k[0] + k[1] + c)/deno
                distance_abs = math.abs(distance)
                if (distance_abs > distance_max):
                    distance_max = distance_abs
                    distance_max_coords = (pt1, test_pt)
                if (distance_max < distance_min_model):
                    distance_min_model = distance_max
                    distance_min_mondel_coords = distance_max_coorsd


    #GET TRANSLATION

    return translation


print("Loading...")

arch1 = Image.open('arch1.png').convert('L')
arch1 = np.array(arch1)
arch1 = np.float32(arch1)

arch2 = Image.open('arch2.png').convert('L')
arch2 = np.array(arch2)
arch2 = np.float32(arch2)


harris_converted_arch1 = corner_detection_algorithm(arch1)
arch1_interest_points = harris_corner_detection(harris_converted_arch1)

harris_converted_arch2 = corner_detection_algorithm(arch2)
arch2_interest_points = harris_corner_detection(harris_converted_arch2)

image_patch_vector_array_arch1 = image_patch_vector(arch1_interest_points, arch1)
image_patch_vector_array_arch2 = image_patch_vector(arch1_interest_points, arch2)

image_patch_vector_array_arch1 = image_patch_vector(arch1_interest_points, arch1)
image_patch_vector_array_arch2 = image_patch_vector(arch1_interest_points, arch2)


translation = (100,100)

response_matrix = response_matrix(image_patch_vector_array_arch1 , image_patch_vector_array_arch2)
print (response_matrix)
composed_image = compose_images(arch1, arch2, translation)





#Generate response matrix by getting the inner product of the harris interest points
#Threshhold to points 0.95 match


#Have list of canditate values and coords
#(x1, y1) ... (xn,yn)
#A model is any two points
#Can calculate distance from point to any line and figure out how good the model is
#e.g least squares etc

#y = mx + c
#ax +by +c = 0
#b = 1
#a = slope
#c = y0 -m*x0



#We should be left with a translation given by two points which the distance between points is minimised
#What we want is to remove the outlier points...





print ("Total Interest points in Arch1: ",)
print (len(arch1_interest_points))

print ("Total Interest points in Arch2: ",)
print (len(arch2_interest_points))


show_images(arch1, arch2, composed_image)
