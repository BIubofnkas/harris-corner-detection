Research and implement Harris Corner Detection using Python/Numpy.  Investigate the behaviour of the algorithm.  Is it rotation invariant?  Is it scale-invariant?
 
Research an appropriate region descriptor for the Harris detector and use it to find correspondence points between pairs of images.  Report on its performance on some test image pairs.  Suitable test images pairs can be found in the Resources section.
 
Provide your Python/Numpy code and a short (3-5 page) report in pdf showing the behaviour of your code.
